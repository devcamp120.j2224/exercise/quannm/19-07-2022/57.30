package com.devcamp.j5730.service;

import java.util.ArrayList;
import java.util.Arrays;

import com.devcamp.j5730.model.Address;
import com.devcamp.j5730.model.Animal;
import com.devcamp.j5730.model.Duck;
import com.devcamp.j5730.model.Fish;
import com.devcamp.j5730.model.Person;
import com.devcamp.j5730.model.Professor;
import com.devcamp.j5730.model.Student;
import com.devcamp.j5730.model.Subject;
import com.devcamp.j5730.model.Worker;
import com.devcamp.j5730.model.Zebra;

public class PersonService {
    public ArrayList<Person> getListStudent() {
        Subject subjectHoaDaiCuong = new Subject("Hóa đại cương", 1,
                                                    new Professor(50, "male", "PhucDM", new Address(), new ArrayList<Animal>(Arrays.asList(
                                                            new Duck(1, "male", "yellow")
                                                    ))));
        ArrayList<Subject> subjects01 = new ArrayList<Subject>();
        subjects01.add(subjectHoaDaiCuong);
        ArrayList<Person> listStudent = new ArrayList<>();
        Student studentQ = new Student(27, "male", "Quan", new Address(), new ArrayList<Animal>(), 1, subjects01);
        Student studentB = new Student(23, "female", "Boi", new Address(), new ArrayList<Animal>(Arrays.asList(
                new Fish(1, "female", 15, true),
                new Zebra(5, "male", true)
        )), 2,
                new ArrayList<Subject>() {
                    {
                        add(new Subject("Vậy lý đại cương", 1,
                                new Professor(54, "male", "HoBa", new Address(), new ArrayList<Animal>(Arrays.asList(
                                        new Duck(1, "male", "yellow")
                                )))));
                        add(new Subject("Toán cao cấp", 1,
                                new Professor(52, "female", "DanhNguyen", new Address(), new ArrayList<Animal>(Arrays.asList(
                                        new Duck(1, "male", "yellow")
                                )))));
                    }
        });
        Student studentT = new Student(20, "female", "Thu", new Address(), new ArrayList<Animal>(Arrays.asList(
                new Fish(2, "male", 20, true),
                new Zebra(7, "female", true)
        )), 3,
                new ArrayList<Subject>(Arrays.asList(
                        new Subject("Anh văn", 1,
                                new Professor(53, "male", "HoangNg", new Address(), new ArrayList<Animal>(Arrays.asList(
                                        new Duck(1, "male", "yellow")
                                )))),
                        new Subject("Sinh học", 1,
                                new Professor(45, "female", "ThuyD", new Address(), new ArrayList<Animal>(Arrays.asList(
                                        new Duck(1, "male", "yellow")
                                )))))));
        listStudent.add(studentQ);
        listStudent.add(studentB);
        listStudent.add(studentT);
        return listStudent;
    }

    public ArrayList<Person> getListWorker() {
        ArrayList<Person> listWorker = new ArrayList<>();
                Worker workerTh = new Worker(51, "female", "ThanhHT", new Address(), new ArrayList<Animal>(Arrays.asList(
                        new Duck(1, "male", "yellow"),
                        new Fish(2, "female")
                )));
                Worker workerD = new Worker(57, "male", "DucNM", new Address(), new ArrayList<Animal>(Arrays.asList(
                        new Duck(2, "female", "white"),
                        new Zebra(6, "male", true)
                )));
        listWorker.add(workerD);
        listWorker.add(workerTh);
        return listWorker;
    }

    public ArrayList<Person> getListProfessor() {
        ArrayList<Person> listProfessor = new ArrayList<>();
        Professor professor1 = new Professor(45, "female", "ThuyD", new Address(), new ArrayList<Animal>(Arrays.asList(
                new Duck(1, "male", "yellow"))));
        Professor professor2 = new Professor(53, "male", "HoangNg", new Address(), new ArrayList<Animal>(Arrays.asList(
                new Duck(1, "male", "yellow"))));
        listProfessor.add(professor1);
        listProfessor.add(professor2);
        return listProfessor;
    }

    public ArrayList<Person> getListAllPerson() {
        ArrayList<Person> listAll = new ArrayList<>();
        listAll.addAll(getListStudent());
        listAll.addAll(getListWorker());
        listAll.addAll(getListProfessor());
        return listAll;
    }
}

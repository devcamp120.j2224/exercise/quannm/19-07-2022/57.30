package com.devcamp.j5730.model;

import java.util.ArrayList;

public class Professor extends Person {
    private int salary;

    public Professor(int age, String gender, String name, Address address, ArrayList<Animal> listPet) {
        super(age, gender, name, address, listPet);
    }

    public Professor(int age, String gender, String name, Address address, ArrayList<Animal> listPet, int salary) {
        super(age, gender, name, address, listPet);
        this.salary = salary;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public void eat() {
        System.out.println("Professor eating...");
    }

    public String teaching() {
        return new String("Professor teaching...");
    }
}
